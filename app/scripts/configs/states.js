/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/*
 * ماشین حالت نرم افزار
 */
.config(function($routeProvider) {
	$routeProvider//
	/**
	 * @ngdoc ngRout
	 * @name /elearn/domain/:id
	 * @description Display a domain with domainId
	 */
	.when('/elearn/domain/:domainId', {
		templateUrl : 'views/elearn-domain.html',
		controller : 'ElearnDomainCtrl'
	})
	/**
	 * @ngdoc ngRout
	 * @name /elearn/topic/:id
	 * @description Display a topic with topicId
	 */
	.when('/elearn/topic/:topicId', {
		templateUrl : 'views/elearn-topic.html',
		controller : 'ElearnTopicCtrl'
	})
	/**
	 * @ngdoc ngRout
	 * @name /elearn/course/:id
	 * @description Display a course with courseId
	 */
	.when('/elearn/course/:courseId', {
		templateUrl : 'views/elearn-course.html',
		controller : 'ElearnCourseCtrl'
	})
	/**
	 * @ngdoc ngRout
	 * @name /elearn/lesson/:lessonId
	 * @description Display a lesson with lessonId
	 */
	.when('/elearn/lesson/:lessonId', {
		templateUrl : 'views/elearn-lesson.html',
		controller : 'ElLessonCtrl'
	})
	/**
	 * @ngdoc ngRout
	 * @name /elearn/part/:partId
	 * @description Display a part with partId
	 */
	.when('/elearn/part/:partId', {
		templateUrl : 'views/elearn-part.html',
		controller : 'ElPartCtrl'
	});
});
