/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')

/**
 * @ngdoc controller
 * @name ElLessonCtrl
 * @description Manage a view of Lesson
 * 
 * @property {Object} ctrl - Options to manage cotroller
 * @property {boolean} ctrl.loadingParts - True if the controller working on parts list
 * @property {boolean} ctrl.loadingLesson - True if the controller working lesson
 * @property {Array} ctrl.items -List of all loaded parts
 * @property {Object} ctrl.error -Last error object
 * @property {boolean} ngEditable -Editable state
 * 
 * 
 * 
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
.controller('ElLessonCtrl', function($scope, $navigator, QueryParameter, $elearn, $routeParams) {

	/*
	 * Handle backend error
	 */
	function _handleError(error) {
		// TODO: handleErro
		ctrl.error = error.data;
	}

	/**
	 * Search fro parts
	 * 
	 * @memberof ElLessonCtrl
	 * @param {string} query -String query to search
	 * @returns {promiss} to search for parts
	 */
	function search(query) {
		QueryParameter.setQuery(query);
		QueryParameter.setPage(0);
		return reload();
	}

	/**
	 * Loading next page of parts
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss} to load page
	 */
	function nextPage() {
		if (ctrl.loadingParts) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			QueryParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.loadingParts = true;
		$scope.lesson.parts(QueryParameter) //
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, _handleError)
		.finally(function() {
			ctrl.loadingParts = false;
		});
	}

	/**
	 * Adds new part
	 * 
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss}
	 */
	function addPart() {
		if (ctrl.loadingParts) {
			return;
		}
		ctrl.loadingParts = true;
		return $elearn.newPart({
			lesson : $scope.lesson.id,
			title : 'Part title',
			description : 'Auto created part',
			order : ctrl.items.length,
			mime_type : 'application/weburger+json'
		}) //
		.then(function(part) {
			ctrl.items.unshift(part);
		})
		.finally(function(){
			ctrl.loadingParts = false;
		});
	}

	/**
	 * Removes a part
	 * 
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss}
	 */
	function removePart() {
	}

	/**
	 * Loads current lesson
	 * 
	 * @memeberof ElLessonCtrl
	 * @returns {promiss} to load lesson
	 */
	function reload() {
		if (ctrl.loadingLesson) {
			return;
		}
		ctrl.loadingLesson = true;
		// TODO: maso, 2018: check if lesson id exist.
		var lessonId = $routeParams.id || $routeParams.lessonId;
		return $elearn.lesson(lessonId) //
			.then(function(lesson) {
				$scope.lesson = lesson;
				requests = null;
				ctrl.items = [];
				return nextPage();
			}, _handleError) //
			.finally(function() {
				ctrl.loadingLesson = false;
			});
	}

	/**
	 * 
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss}
	 */
	function remove() {
		return $scope.lesson.delete() //
			.then(function() {
				$navigator.openPage('elearning/courses/'+$scope.lesson.course);
			}, function(error) {
				alert('Fail to remove course:' + error.data.emessage);
			});
	}

	/**
	 * 
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss}
	 */
	function update() {
		return $scope.lesson.update() //
			.then(function() {
				//			$navigator.openPage('elearning/courses/'+$scope.lesson.course);
			}, function(error) {
				alert('Fail to remove lesson:' + error.data.emessage);
			});
	}


	/**
	 * Pagination parameter used to list parts
	 * 
	 * @memberof ElLessonCtrl
	 * @type QueryParameter
	 */
	var QueryParameter = new QueryParameter();
	QueryParameter.setOrder('id', 'd');
	var requests = null;
	// Controller options
	var ctrl = {
		items : [],
		sortKeys : [
			'id',
			'name',
			'description' ],
		actions : [ {
			title : 'New parts',
			icon : 'add',
			action : addPart
		} ]
	};
	$scope.ctrl = ctrl;
	
	// Parts
	$scope.items = [];
	$scope.QueryParameter = QueryParameter;

	$scope.search = search;
	$scope.nextPage = nextPage;
	$scope.removePart = removePart;
	$scope.addPart = addPart;

	// lesson
	$scope.reload = reload;
	$scope.remove = remove;
	$scope.update = update;



	//	/*
	//	 * Add scope menu
	//	 */
	//	$app //
	//	.scopeMenu($scope) //
	//	.add({ // edit menu
	//		priority : 15,
	//		icon : 'edit',
	//		label : 'Edit content',
	//		tooltip : 'Toggle edit mode of the current contetn',
	//		visible : function() {
	//			return $scope.app.user.owner;
	//		},
	//		action : function(){
	//			$scope.ngEditable = !$scope.ngEditable;
	//		}
	//	}) //
	//	.add({ // create new menu
	//		priority : 15,
	//		icon : 'add_box',
	//		label: 'Create',
	//		tooltip: 'Creates new content page',
	//		visible : function() {
	//			return scope.app.user.owner;
	//		},
	//		action : createContent
	//	})//
	//	.add({ // clone new menu
	//		priority : 15,
	//		icon : 'content_copy',
	//		label: 'Clone',
	//		tooltip: 'Clone current content page',
	//		visible : function() {
	//			return scope.app.user.owner;
	//		},
	//		action : cloneContent
	//	})//
	//	.add({ // save menu
	//		priority : 10,
	//		icon : 'save',
	//		label : 'Save current changes',
	//		tooltip : 'Save current changes.',
	//		visible : function() {
	//			return $scope.app.user.owner;
	//		},
	//		action : savePart
	//	});

	reload();
});