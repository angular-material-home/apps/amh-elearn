/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/**
 * 
 */
.controller('ElearnDomainCtrl', function($scope, $navigator, QueryParameter, $elearn, $routeParams) {

	var QueryParameter = new QueryParameter();
	QueryParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state : 'relax',
			items : []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param QueryParameter
	 * @returns
	 */
	function find(query) {
		QueryParameter.setQuery(query);
		QueryParameter.setPage(0);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.state !== 'relax') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			QueryParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'loading-list';
		$scope.domain.topics(QueryParameter) //
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.state = 'relax';
		}, function(error) {
			ctrl.error = error;
			ctrl.state = 'error';
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload() {
		if (ctrl.state !== 'relax') {
			return;
		}
		ctrl.state = 'loading';
		$elearn.domain($routeParams.domainId) //
		.then(function(domain) {
			$scope.domain = domain;
			requests = null;
			ctrl.items = [];
			ctrl.state = 'relax';
			nextPage();
		}, function(error) {
			$scope.domain = null;
			ctrl.state = 'error';
			ctrl.error = error;
		});
	}

	/**
	 * Remove domain
	 */
	function removeDomain() {
		return $scope.domain.delete() //
		.then(function() {
			$navigator.openPage('elearning/domains');
		}, function(error) {
			alert('Fail to remove domain:' + error.data.emessage);
		});
	}

	/**
	 * Update current domain
	 */
	function updateDoamin() {
		return $scope.domain.update() //
		.then(function() {
			//			$$navigator.openPage('elearning/domains');
		}, function(error) {
			alert('Fail to remove domain:' + error.data.emessage);
		});
	}


	/**
	 * Create a new topic
	 * 
	 * A new topic with pre-defined value is created and added into the list.
	 */
	function addTopic() {
		if (ctrl.status === 'working') {
			return;
		}
		ctrl.status = 'working';
		return $elearn.newTopic({
			domain : $scope.domain.id,
			title : 'Topic title',
			description : 'Auto created topic',
			cover : $scope.domain.cover
		}) //
		.then(function(topic) {
			ctrl.status = 'relax';
			ctrl.items.unshift(topic);
		});
	}

	/**
	 * Remove topic
	 * 
	 */
	function removeTopic(/*topic*/) {
		// XXX: maso, 2017: maso
	}


	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = removeDomain;
	$scope.update = updateDoamin;

	$scope.addTopic = addTopic;
	$scope.removeTopic = removeTopic;

	$scope.ctrl = ctrl;


	// Pagination toolbar
	$scope.QueryParameter = QueryParameter;
	$scope.reload = reload;
	$scope.sortKeys = [
		'id',
		'name',
		'description'
		];
	$scope.moreActions = [ {
		title : 'New topc',
		icon : 'add',
		action : addTopic
	} ];

	reload();
});