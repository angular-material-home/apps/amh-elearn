/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/**
 * 
 */
.controller('ElPartsCtrl', function($scope, $navigator, QueryParameter, $elearning) {

	var QueryParameter = new QueryParameter();
	QueryParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'ok',
			items: []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param QueryParameter
	 * @returns
	 */
	function find(query) {
		QueryParameter.setQuery(query);
		QueryParameter.setPage(0);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			QueryParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$elearning.parts(QueryParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'ok';
		}, function() {
			ctrl.status = 'fail';
		});
	}

	function addDomain(){
		$navigator.openPage('elearning/parts/new');
	}


	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(object) {
		return object.delete()//
		.then(function(){
			var index = ctrl.items.indexOf(object);
			if (index > -1) {
				ctrl.items.splice(index, 1);
			}
		});
	}

	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.add = addDomain;

	$scope.ctrl = ctrl;


	// Pagination toolbar
	$scope.QueryParameter = QueryParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.moreActions=[{
		title: 'New domain',
		icon: 'add',
		action: addDomain
	}];
});