/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/**
 * @ngdoc controller
 * @name ElPartCtrl
 * @description Part controller
 * 
 * @property {Object} ctrl - Options to manage cotroller
 * @property {boolean} ctrl.loadingPart - True if the controller working on part list
 * @property {Object} ctrl.error -Last error object
 * @property {boolean} ngEditable -Editable state
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir> 
 */
.controller('ElPartCtrl', function($scope, $navigator, $elearn, $routeParams, $app, QueryParameter, $actions) {
	/*
	 * Handle backend error
	 */
	function _handleError(error) {
		// TODO: handleErro
		ctrl.error = error.data;
	}

	/*
	 * Check if possible to edit
	 */
	function _canEdit() {
		return $scope.app.user.owner;
	}
	
	/**
	 * Load lesson data
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to load the lesson of the part
	 */
	function loadLesson(){
		if(ctrl.loadingLesson){
			return;
		}
		ctrl.loadingLesson = true;
		return $elearn.lesson($scope.part.lesson)//
		.then(function(lesson){
			$scope.lesson = lesson;
			return nextPage();
		}, _handleError)
		.finally(function(){
			ctrl.loadingLesson = false;
		});
	}
	

	/**
	 * Loading next page of parts
	 * 
	 * @memberof ElLessonCtrl
	 * @return {promiss} to load page
	 */
	function nextPage() {
		if (ctrl.loadingParts || !$scope.lesson) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			QueryParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.loadingParts = true;
		$scope.lesson.parts(QueryParameter) //
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, _handleError)
		.finally(function() {
			ctrl.loadingParts = false;
		});
	}

	/**
	 * Load content of the part
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to load content of the part
	 */
	function loadContent(){
		return $scope.part.value()//
		.then(function(value){
			$scope.model = value;
			if(!angular.isObject(value)){
				$scope.model = {};
			}
//			toast('Part content is loaded.');
		}, _handleError);
	}

	/**
	 * Save content of the part
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to save content of the part
	 */
	function saveContent(){
		return $scope.part.setValue($scope.model)//
		.then(function(){
			toast('Part is saved.');
		}, _handleError);
	}

	/**
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to
	 */
	function reload(){
		return $elearn.part($routeParams.partId)//
		.then(function(part){
			$scope.part = part;
			ctrl.items = [];
			loadLesson();
			return loadContent();
		}, _handleError);
	}

	/**
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to
	 */
	function remove(){
		return $scope.part.delete()//
		.then(function(){
			$navigator.openPage('elearning/lesson/'+$scope.part.lesson);
		}, _handleError);
	}


	/**
	 * 
	 * @memberof ElPartCtrl
	 * @return {promiss} to
	 */
	function update(){
		return $scope.part.update()//
		.then(function(){
			toast('Part is updated.');
		}, _handleError);
	}
	
	/**
	 * 
	 */
	var QueryParameter = new QueryParameter();
	QueryParameter.setOrder('id', 'd');
	var requests = null;
	$scope.QueryParameter = QueryParameter;
	// controllers
	var ctrl = {
			items: []
	};
	
	$scope.reload = reload;
	$scope.remove = remove;
	$scope.update = update;

	$scope.saveContent = saveContent;
	$scope.loadContent = loadContent;

	$scope.ctrl = ctrl;


	/*
	 * Add scope menu
	 */

	/*
	 * Add scope menus
	 */
	$actions.newAction({ // edit menu
		id: 'amh.elear.part.edit',
		priority : 15,
		icon : 'edit',
		title : 'Edit',
		description : 'Toggle edit mode of the current part',
		visible : _canEdit,
		action : function() {
			$scope.ngEditable = !$scope.ngEditable;
		},
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	}); //

	$actions.newAction({ // save menu
		id: 'amh.elearn.part.save',
		priority : 10,
		icon : 'save',
		title : 'Save',
		description : 'Save current changes',
		visible : _canEdit,
		action : saveContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	reload();
});

