/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/**
 * 
 */
.controller('ElearnCourseCtrl', function($scope, $navigator, QueryParameter, $elearn, $routeParams) {

	var QueryParameter = new QueryParameter();
	QueryParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'ok',
			items: []
	};


	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param QueryParameter
	 * @returns
	 */
	function find(query) {
		QueryParameter.setQuery(query);
		QueryParameter.setPage(0);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			QueryParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$scope.course.lessons(QueryParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'ok';
		}, function(error) {
			ctrl.error = error;
			ctrl.status = 'fail';
		});
	}


	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		$elearn.course($routeParams.id || $routeParams.courseId)//
		.then(function(course){
			$scope.course = course;
			requests = null;
			ctrl.items = [];
			$elearn.topic(course.topic)//
			.then(function(topic){
				$scope.topic = topic;
			});
			nextPage();
		}, function(error){
			$scope.course = null;
			ctrl.status = 'notFound';
			ctrl.error = error;
		});
	}


	function remove(){
		return $scope.course.delete()//
		.then(function(){
//			$navigator.openPage('elearning/topics/'+$scope.course.topic);
		}, function(error){
			alert('Fail to remove course:' + error.data.emessage);
		});
	}
	
	function update(){
		return $scope.course.update()//
		.then(function(){
//			$navigator.openPage('elearning/topics/'+$scope.course.topic);
		}, function(error){
			alert('Fail to remove course:' + error.data.emessage);
		});
	}
	
	/**
	 * Add lesson
	 */
	function addLesson(){
		if (ctrl.status === 'working') {
			return;
		}
		ctrl.status = 'working';
		return $elearn.newLesson({
			course : $scope.course.id,
			title : 'Lesson title',
			description : 'Auto created lesson',
			cover : $scope.course.cover
		}) //
		.then(function(object) {
			ctrl.status = 'relax';
			ctrl.items.unshift(object);
		});
	}
	
	/**
	 * Remove lesson
	 */
	function removeLesson(/*lesson*/){}

	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.update = update;
	
	$scope.addLesson = addLesson;
	$scope.removeLesson = removeLesson;

	$scope.ctrl = ctrl;


	// Pagination toolbar
	$scope.QueryParameter = QueryParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.moreActions=[{
		title: 'New lesson',
		icon: 'add',
		action: addLesson
	}];
	
	reload();
});
