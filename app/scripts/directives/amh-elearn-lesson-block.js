/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhElearn')
/**
 * @description Adding preloading page
 */
.directive('amhElearnLessonBlock', function(QueryParameter) {

	/**
	 * Adding preloader.
	 * 
	 * @param scope
	 * @param element
	 * @param attr
	 * @returns
	 */
	function postLink(scope, element, attr, ngModel) {
		
		var QueryParameter = new QueryParameter();
		QueryParameter.setOrder('id', 'd');
		var requests = null;
		var ctrl = {
				state: 'ok',
				items: []
		};

		ngModel.$render = function(){
			scope.lesson = ngModel.$viewValue;
			nextPage();
			ctrl.items = [];
		};
		/**
		 * لود کردن داده‌های صفحه بعد
		 * 
		 * @returns
		 */
		function nextPage() {
			if (ctrl.status === 'loading') {
				return;
			}
			if (requests && !requests.hasMore()) {
				return;
			}
			if (requests) {
				QueryParameter.setPage(requests.next());
			}
			ctrl.status = 'loading';
			scope.lesson.parts(QueryParameter)//
			.then(function(items) {
				requests = items;
				ctrl.items = ctrl.items.concat(requests.items);
			}, function(error) {
				ctrl.error = error;
			})//
			.finally(function(){
				ctrl.status = 'ok';
			});
		}
		
		scope.nextPage = nextPage;
		scope.ctrl = ctrl;
	}

	return {
		restrict : 'E',
		transclude : false,
		replace: false,
		require: '?ngModel',
		templateUrl: 'views/directives/amh-elearn-lesson-block.html',
		scope: {},
		link: postLink
	};
});